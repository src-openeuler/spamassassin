Name:             spamassassin
Version:          4.0.1
Release:          2
Summary:          A program used for e-mail spam filtering
License:          Apache-2.0
URL:              https://spamassassin.apache.org/
Source0:          https://www.apache.org/dist/%{name}/source/Mail-SpamAssassin-%{version}.tar.bz2
Source1:          https://www.apache.org/dist/%{name}/source/Mail-SpamAssassin-rules-%{version}.r1916528.tgz
Source2:          openeuler_local.cf
Source3:          spamassassin-default.rc
Source4:          spamassassin-spamc.rc
Source5:          spamassassin.sysconfig
Source6:          sa-update.logrotate
Source7:          sa-update.crontab
Source8:          sa-update.cronscript
Source9:          sa-update.force-sysconfig
Source10:         spamassassin-helper.sh
Source11:         spamassassin-official.conf
Source12:         spamassassin.service
Source13:         sa-update.service
Source14:         sa-update.timer

Patch0001:        spamassassin-4.0.0-gnupg2.patch
Patch0002:        spamassassin-4.0.0-add-logfile-homedir-options.patch

BuildRequires:    gcc openssl-devel perl-devel perl-generators perl-HTML-Parser >= 3.43
BuildRequires:    perl-interpreter >= 2:5.8.0 perl(Archive::Tar) perl(DB_File)
BuildRequires:    perl(Digest::SHA1) perl(HTML::Parser) perl(IO::Socket::INET6)
BuildRequires:    perl(IO::Socket::SSL) perl(LWP::UserAgent) perl(Mail::DKIM)
BuildRequires:    perl(Mail::SPF) perl(Net::CIDR::Lite) perl(Net::DNS) perl(NetAddr::IP)
BuildRequires:    perl(Test::More) perl(Time::HiRes) systemd-units

Requires:         gnupg2 procmail perl-HTML-Parser >= 3.43 systemd-pam
Requires:         perl(Archive::Tar) perl(BSD::Resource) perl(DB_File) perl(Data::Dumper)
Requires:         perl(Digest::MD5) perl(Digest::SHA) perl(Encode::Detect) perl(Errno)
Requires:         perl(Exporter) perl(HTTP::Date) perl(IO::Socket::INET6) perl(IO::Socket::SSL)
Requires:         perl(LWP::UserAgent) perl(List::Util) perl(Mail::DKIM) perl(Mail::SPF)
Requires:         perl(Net::CIDR::Lite) perl(Net::DNS) perl(Socket) perl(Time::HiRes)

Requires(post):   diffutils systemd-sysv
%{?systemd_requires}

%description
Apache SpamAssassin is the Open Source anti-spam platform giving system administrators
a filter to classify email and block spam (unsolicited bulk email).

It uses a robust scoring framework and plug-ins to integrate a wide range of advanced
heuristic and statistical analysis tests on email headers and body text including text
analysis, Bayesian filtering, DNS blocklists, and collaborative filtering databases.

%package        help
Summary:        Help documents for %{name}
Buildarch:      noarch

%description    help
Man pages and other help documents for %{name}.

%prep
%autosetup -n Mail-SpamAssassin-%{version} -p1

%build
export CFLAGS="$RPM_OPT_FLAGS"
export LDFLAGS="%{build_ldflags}"
perl Makefile.PL DESTDIR=%buildroot SYSCONFDIR=%{_sysconfdir} INSTALLDIRS=vendor ENABLE_SSL="yes" < /dev/null
%make_build OPTIMIZE="$RPM_OPT_FLAGS"

%install
%makeinstall PREFIX=%buildroot/%{prefix} INSTALLMAN1DIR=%buildroot%{_mandir}/man1 \
             INSTALLMAN3DIR=%buildroot%{_mandir}/man3 LOCAL_RULES_DIR=%{buildroot}/etc/mail/spamassassin
chmod 755 %buildroot%{_bindir}/*

install -d %buildroot%{_sysconfdir}/mail/spamassassin
install -d %buildroot%{_sysconfdir}/sysconfig
install -d %buildroot%{_sysconfdir}/logrotate.d
install -d %buildroot%{_unitdir}
install -m 0644 %{SOURCE2}  %buildroot%{_sysconfdir}/mail/spamassassin/local.cf
install -m 0644 %{SOURCE5}  %buildroot%{_sysconfdir}/sysconfig/spamassassin
install -m 0644 %{SOURCE3}  %{SOURCE4}  %buildroot/etc/mail/spamassassin
install -m 0755 %{SOURCE10} %buildroot/etc/mail/spamassassin
install -m 0644 %{SOURCE6}  %buildroot/etc/logrotate.d/sa-update
install -m 0644 %{SOURCE9}  %buildroot%{_sysconfdir}/sysconfig/sa-update
install -m 0744 %{SOURCE8}  %buildroot%{_datadir}/spamassassin/sa-update.cron
install -m 0644 %{SOURCE12} %buildroot%{_unitdir}/spamassassin.service
install -m 0644 %{SOURCE13} %buildroot%{_unitdir}/sa-update.service
install -m 0644 %{SOURCE14} %buildroot%{_unitdir}/sa-update.timer

[ -x /usr/lib/rpm/brp-compress ] && /usr/lib/rpm/brp-compress

find %buildroot \( -name perllocal.pod -o -name .packlist \) -exec rm -v {} \;
find %buildroot -type d -depth -exec rmdir {} 2>/dev/null ';'

pushd %buildroot%{_datadir}/spamassassin/
tar xfvz %{SOURCE1}
sed -i -e 's|\@\@VERSION\@\@|3.004002|' *.cf
popd

install -d                     %buildroot%{_localstatedir}/lib/spamassassin
install -d -m 0700             %buildroot%{_sysconfdir}/mail/spamassassin/sa-update-keys/
install -d -m 0755             %buildroot%{_sysconfdir}/mail/spamassassin/channel.d/
install    -m 0644 %{SOURCE11} %buildroot%{_sysconfdir}/mail/spamassassin/channel.d/

%post
%systemd_post spamassassin.service
%systemd_post sa-update.timer

TMPFILE=$(/bin/mktemp /etc/sysconfig/spamassassin.XXXXXX) || exit 1
cp /etc/sysconfig/spamassassin $TMPFILE
perl -p -i -e 's/(["\s]-\w+)a/$1/ ; s/(["\s]-)a(\w+)/$1$2/ ; s/(["\s])-a\b/$1/' $TMPFILE
perl -p -i -e 's/ --auto-whitelist//' $TMPFILE
cmp /etc/sysconfig/spamassassin $TMPFILE || cp $TMPFILE /etc/sysconfig/spamassassin
rm $TMPFILE

if [ -f /etc/spamassassin.cf ]; then
        mv /etc/spamassassin.cf /etc/mail/spamassassin/migrated.cf
fi
if [ -f /etc/mail/spamassassin.cf ]; then
        mv /etc/mail/spamassassin.cf /etc/mail/spamassassin/migrated.cf
fi

%postun
%systemd_postun spamassassin.service
%systemd_postun sa-update.timer

%preun
%systemd_preun spamassassin.service
%systemd_preun sa-update.timer
%triggerun -- spamassassin < 3.3.2-2
%{_bindir}/systemd-sysv-convert --save spamassassin >/dev/null 2>&1 ||:

%files
%doc LICENSE
%{_bindir}/*
%dir %{_sysconfdir}/mail
%config(noreplace) %{_sysconfdir}/mail/spamassassin
%config(noreplace) %{_sysconfdir}/logrotate.d/sa-update
%config(noreplace) %{_sysconfdir}/sysconfig/*
%dir %{_datadir}/spamassassin
%{_datadir}/spamassassin/*
%dir %{_localstatedir}/lib/spamassassin
%{perl_vendorlib}/spamassassin-run.pod
%dir %{perl_vendorlib}/Mail
%dir %{perl_vendorlib}/Mail/SpamAssassin
%dir %{perl_vendorlib}/Mail/SpamAssassin/*
%dir %{perl_vendorlib}/Mail/SpamAssassin/Message/Metadata
%{perl_vendorlib}/Mail/*.pm
%{perl_vendorlib}/Mail/SpamAssassin/*.pm
%{perl_vendorlib}/Mail/SpamAssassin/*/*.pm
%{perl_vendorlib}/Mail/SpamAssassin/Message/Metadata/*.pm
%{perl_vendorlib}/Mail/SpamAssassin/Pyzor/Digest/*.pm
%{_unitdir}/*

%files help
%doc Changes CREDITS NOTICE README TRADEMARK UPGRADE USAGE sample-nonspam.txt sample-spam.txt
%{_mandir}/man[13]/*

%changelog
* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 4.0.1-2
- drop useless perl(:MODULE_COMPAT) requirement
- harden systemd unit service

* Wed Jul 31 2024 wangkai <13474090681@163.com> - 4.0.1-1
- Update to 4.0.1
- It provides compatability with the latest version of Perl 5.38

* Thu Apr 13 2023 yaoxin <yao_xin001@hoperun.com> - 4.0.0-1
- Update to 4.0.0

* Tue Dec 13 2022 liyanan <liyanan32@h-partners.com>  - 3.4.5-2
- add requires systemd-pam

* Wed Mar 31 2021 wangxiao <wangxiao65@huawei.com> - 3.4.5-1
- Update to 3.4.5, fix CVE-2020-1946

* Sun Feb 7 2021 zhanghua <zhanghua40@huawei.com> - 3.4.4-1
- update to 3.4.4 to fix CVE-2020-1930 CVE-2020-1931

* Mon Dec 09 2019 zhouyihang <zhouyihang1@huawei.com> - 3.4.2-4
- Package init
